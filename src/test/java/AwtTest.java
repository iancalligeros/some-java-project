import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

public class AwtTest {
    public Integer int1 = 1;
    public Integer int2 = 2;
    public String string1 = "TEST";

    @Before
    public void setup() {
    }

    @Test
    public void test1() {
        Assert.assertTrue(int1.equals(1));
    }

    @Test
    public void test2() {
        Assert.assertTrue(int2.equals(2));
    }

    @Test
    public void test3() {
        Assert.assertTrue(string1.equals("TEST"));
    }
}
